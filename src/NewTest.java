import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import java.io.File;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class NewTest {
	private WebDriver driver;
	static Scanner sc = new Scanner(System.in);
	// username is the email id we will be using for verification
	// password is the password corresponding to the email id, have to be given as input by the user
	private static String username = null;
	private static String pwd = null;
	private String text = "Hi ! This is my first test message using Virtru. Hope I get expected results";
	
	
/*The test below is for automating the 2nd part of the Assignment for composing the email using Virtru encryption*
 * /	
 /*@Test (priority=0)
  public void composeEmail() throws InterruptedException {
	    driver.findElement(By.linkText("Skip, I already know how to use Virtru")).click();
		driver.findElement(By.xpath("//div[@class='z0']/div")).click();
		driver.findElement(By.xpath("//div[@class='virtru-slider']/div")).click();
		driver.findElement(By.xpath("//a[@class ='virtru-firsttime-activation-button virtru-activation-button']")).click();
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.switchTo().window(winHandleBefore);
		driver.findElement(By.xpath("//textarea[@name='to']")).sendKeys(username);
		driver.findElement(By.xpath("//input[@name='subjectbox']")).sendKeys("Hi");
		Thread.sleep(5000);
		WebElement body = driver.findElement(By.xpath("//div[@aria-label='Message Body']"));
		body.click();
		body.clear();
		body.sendKeys(text);
		driver.findElement(By.xpath("//div[contains(text(),'Send')]")).click();
		Thread.sleep(5000);
  }
 */
	
/*This test is used for verifying the email contents sent in the previous step no. 2 per assignment*/
  @Test (priority=1)
  public void assertTextEmail() throws InterruptedException {
	//  driver.findElement(By.linkText("Skip, I already know how to use Virtru")).click();  
	  // Above line is commented only in case of incognito mode, for normal execution, please uncomment the line no. 56s
	  List<WebElement> unreademail = driver.findElements(By.cssSelector("div.xT>div.y6>span>b")); //finding unread emails
	  String MyText = "Hi"; //expected subject of the email to be verified
	  for(int i=0;i<unreademail.size();i++){
		        if(unreademail.get(i).getText().contains(MyText)){
		        	unreademail.get(i).click();
		        	driver.findElement(By.partialLinkText("Unlock")).click();
		        	Thread.sleep(15000);
		        	String winHandleBefore = driver.getWindowHandle();
		            for(String winHandle : driver.getWindowHandles()) {
			    		    driver.switchTo().window(winHandle);
			    		    }
			    	driver.findElement(By.xpath("//div[@data-email='"+username+"']")).click();
			   	    driver.findElement(By.xpath("//a[@class='btn btn-lg auth-choice-btn sendEmailButton']")).click();
		 		    Thread.sleep(5000);
	    		    driver.close();
		   		    driver.switchTo().window(winHandleBefore);
			        driver.findElement(By.partialLinkText("Inbox")).click();
			        List<WebElement> emailSubjectList = driver.findElements(By.cssSelector("div.xT>div.y6>span>b"));
				 	for(WebElement emailsub : emailSubjectList){
				        if(emailsub.getText().contains("Verify with Virtru") == true){
				       	   emailsub.click();
				    	   driver.findElement(By.partialLinkText("VERIFY ME")).click();
		    		       for(String winHandle : driver.getWindowHandles()){
				   	   		    	driver.switchTo().window(winHandle);
				   		        }
				   	 		 String actualBody = driver.findElement(By.xpath("//span[@class='tdf-body']//div")).getText();
				   		     Assert.assertEquals(actualBody,text);
				   	     break;
				   		    }
				      }
				   break;  
				  }
				}}
		    
	  
  
  @BeforeTest
  public void loginGmail() throws InterruptedException {
	    System.out.println("Enter username");
	    username=sc.nextLine();
	    System.out.println("Enter Password");
	    pwd=sc.nextLine();
	    sc.close();
		System.setProperty("webdriver.chrome.driver","C:\\Users\\gitanshi\\Downloads\\chromedriver_win32\\chromedriver.exe");
		// The path in line 98 has to be modified according to the directory you have your chromedriver.exe
		ChromeOptions options = new ChromeOptions(); 
		options.addExtensions(new File("E:\\7.5.3.0_0.crx"));
		//The path in line 101 has to be customized according to where the user has their crx file for Virtru extension
		options.addArguments("--incognito");
		driver = new ChromeDriver(options);
		String url = "https://www.gmail.com";
	    Thread.sleep(5000);
		synchronized(driver) {
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			}
		driver.get(url);	
		WebElement email = driver.findElement(By.xpath("//input[@id='identifierId']"));
		email.sendKeys(username);
		driver.findElement(By.id("identifierNext")).click();
		WebElement password = driver.findElement(By.xpath("//input[@name='password']"));
		password.sendKeys(pwd);
		driver.findElement(By.id("passwordNext")).click();
		Thread.sleep(5000);
  }

  @AfterTest
  public void afterTest() {
	  driver.close();
  }

}
