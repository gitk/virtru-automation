import java.io.File;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Test {
	public static void main(String[] args) throws InterruptedException {
	Scanner sc = new Scanner(System.in);
	System.out.println("Enter username");
	String username = sc.nextLine();
	System.out.println("Enter Password");
	String pwd = sc.nextLine();
	sc.close();
	System.setProperty("webdriver.chrome.driver","C:\\Users\\gitanshi\\Downloads\\chromedriver_win32\\chromedriver.exe");
	String url = "https://www.gmail.com";
	ChromeOptions options = new ChromeOptions(); 
	options.addExtensions(new File("E:\\7.5.3.0_0.crx"));
	WebDriver driver = new ChromeDriver(options);
	Thread.sleep(5000);
	synchronized(driver) {
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		}
	driver.get(url);	
	WebElement email = driver.findElement(By.xpath("//input[@id='identifierId']"));
	email.sendKeys(username);
	driver.findElement(By.id("identifierNext")).click();
	WebElement password = driver.findElement(By.xpath("//input[@name='password']"));
	password.sendKeys(pwd);
	driver.findElement(By.id("passwordNext")).click();
	driver.findElement(By.linkText("Skip, I already know how to use Virtru")).click();
	driver.findElement(By.xpath("//div[@class='z0']/div")).click();
	driver.findElement(By.xpath("//div[@class='virtru-slider']/div")).click();
	driver.findElement(By.xpath("//a[@class ='virtru-firsttime-activation-button virtru-activation-button']")).click();
	// Store the current window handle
	String winHandleBefore = driver.getWindowHandle();

	// Perform the click operation that opens new window

	// Switch to new window opened
	for(String winHandle : driver.getWindowHandles()){
	    driver.switchTo().window(winHandle);
	}

	// Perform the actions on new window

	// Close the new window, if that window no more required
	//driver.close();

	// Switch back to original browser (first window)
	driver.switchTo().window(winHandleBefore);
	driver.findElement(By.xpath("//textarea[@name='to']")).sendKeys(username);
	driver.findElement(By.xpath("//input[@name='subjectbox']")).sendKeys("Hi");
	Thread.sleep(5000);
	WebElement body = driver.findElement(By.xpath("//div[@aria-label='Message Body']"));
	body.click();
	body.clear();
	body.sendKeys("Hi ! This is my first test message using Virtru. Hope I get expected results");
	driver.findElement(By.xpath("//div[contains(text(),'Send')]")).click();
	//driver.findElement(By.xpath("//div[text()='Send']")).click();
	
	//driver.close();
	}
}
